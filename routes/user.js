// Dependencies
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// [Routes]

// Check if Existing Email
router.post("/checkEmail", userController.checkEmail);

// User Registration
router.post("/register", userController.registerUser);

// User Login
router.post("/login", userController.loginUser);

// User Details
router.post("/profile", verify, userController.getProfile);

// Change User to Admin (Admin Only)
router.put(
  "/updateAdmin/:userId",
  verify,
  verifyAdmin,
  userController.updateUserAsAdmin
);

// Password Reset
router.post("/resetPassword", verify, userController.resetPassword);

// Update User Profile
router.put("/updateProfile", verify, userController.updateProfile);

module.exports = router;
