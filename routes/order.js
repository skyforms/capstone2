// Dependencies
const express = require("express");
const orderController = require("../controllers/order");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// Create Order (Non-Admin Users)
router.post("/createOrder", verify, orderController.createOrder);

// Checkout Order (Change status to "processing")
router.put("/checkout", verify, orderController.checkoutOrder);

// Retrieve Authenticated User's Pending Order
router.get("/myCart", verify, orderController.getMyCart);

// Retrieve All of Authenticated User's Orders
router.get("/myOrders", verify, orderController.getUserOrders);

// Retrieve All Orders (Admin Only)
router.get("/all", verify, verifyAdmin, orderController.getAllOrders);

// Get order status by order ID (admin only)
router.get(
  "/:orderId/status",
  verify,
  verifyAdmin,
  orderController.getOrderStatus
);

// Update Order Status
router.put(
  "/:orderId/status",
  verify,
  verifyAdmin,
  orderController.updateOrderStatus
);

// ADD TO CART FUNCTIONALITIES
// Add to Cart
router.post("/addToCart", verify, orderController.addToCart);

// Update Cart
router.put("/myCart", verify, orderController.updateCart);

// Remove Products from Cart
router.delete(
  "/removeFromCart/:productId",
  verify,
  orderController.removeFromCart
);

// Clear Cart (Non Admin Only)
router.delete("/clearCart", verify, orderController.clearCart);

module.exports = router;
