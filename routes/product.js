// Dependencies
const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// [Routes]
// Create Product (Admin Only)
router.post("/", verify, verifyAdmin, productController.createProduct);

// Retrieve All Products (Admin Only)
router.get("/all", productController.getAllProducts);

// Retrieve All Active Products
router.get("/", productController.getAllActiveProducts);

// Retrieve Specific Product
router.get("/:productId", productController.getProduct);

// Update a Specific Product (Admin Only)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// Archive Specific Product (Admin Only)
router.put(
  "/:productId/archive",
  verify,
  verifyAdmin,
  productController.archiveProduct
);

// Activate Specific Product (Admin Only)
router.put(
  "/:productId/activate",
  verify,
  verifyAdmin,
  productController.activateProduct
);

// Search Products by Name
router.post("/search", productController.searchProductsByName);

module.exports = router;
