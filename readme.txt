## ECOMMERCE API DOCUMENTATION

Hey there, thanks for checking out my work!

This project is a backend e-commerce API for a stationery shop. This documentation contains the test accounts and different routes that you can use to explore this project. 

*** Test Accounts ***

Admin User:
    "email": "haruharu@clover.com",
    "password": "pass1234",

Regular User: 
    "email": "orca@iwatobikou.com",
    "password": "diffpass1234",

*** Routes ***

User Routes

User Registration
Endpoint: POST http://localhost:4000/users/register
Request Body:
{
  "firstName": "string",
  "lastName": "string",
  "email": "string",
  "password": "string",
  "mobileNo": "string",
  "address": "string"
}

User Authentication
Endpoint: POST http://localhost:4000/users/login
Request Body:
{
  "email": "string",
  "password": "string"
}

Change User to Admin
Endpoint: PUT http://localhost:4000/users/updateAdmin/:userId

Reset Password
Endpoint: POST http://localhost:4000/users/resetPassword
Request Body:
{  
  "newPassword": "string"
}

Update Profile
Endpoint: PUT http://localhost:4000/users/profile

---

Product Routes

Create Product (Admin Only)
Endpoint: POST http://localhost:4000/products/
Request Body:
{
  "name": "string",
  "description": "string",
  "price": "number",
  "quantity": "number"
}

Retrieve All Products (Admin Only)
Endpoint: GET http://localhost:4000/products/all

Retrieve All Active Products
Endpoint: GET http://localhost:4000/products/

Update Specific Product (Admin Only)
Endpoint: PUT http://localhost:4000/products/:productId
Request Body:
{
  "name": "string",
  "description": "string",
  "price": "number",
  "quantity": "number"
}

Archive Specific Product (Admin Only)
Endpoint: PUT http://localhost:4000/products/:productId/archive

Activate Specific Product (Admin Only)
Endpoint: PUT http://localhost:4000/products/:productId/activate

Search for Product by name
Endpoint: POST http://localhost:4000/products/search
Request Body:
{
    "productName": "string"
}

--- 

Order Routes

Create Order (Non Admin Only)
Endpoint: POST http://localhost:4000/orders/createOrder
Request Body:
[
  {
    "productId": "string",
    "quantity": "number"
  },
  {
    "productId": "string",
    "quantity": "number"
  }
]

Checkout Order
Endpoint: PUT http://localhost:4000/orders/:orderId

Retrieve Authenticated User's Orders
Endpoint: GET http://localhost:4000/orders/myOrders

Retrieve All Orders (Admin Only)
Endpoint: GET http://localhost:4000/orders/all

Add to Cart
Endpoint: POST http://localhost:4000/orders/addToCart
Request Body:
  {
    "productId": "string",
    "quantity": "number"
  }

Update Cart
Endpoint: PUT http://localhost:4000/orders/myCart
{
  "productId": "string",
  "quantity": "number"
}

Remove From Cart
Endpoint: DELETE http://localhost:4000/orders/removeFromCart/:productId

### END OF DOCUMENTATION