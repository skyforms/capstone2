// Dependencies
const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create Order (New Cart)
module.exports.createOrder = async (req, res) => {
  try {
    // Get user ID from the authenticated user (JWT)
    const userId = req.user.id;

    // Retrieve user's first/last name from Users collection
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    // Extract product data from the request body (if provided)
    const cartItems = req.body || [];

    // Populate product names and calculate total amount based on product prices and quantities
    let totalAmount = 0;
    const populatedProducts = [];
    const wrongProductIds = []; // Array to track wrong product IDs

    for (const item of cartItems) {
      const productDetails = await Product.findById(item.productId);
      if (productDetails) {
        totalAmount += productDetails.price * item.quantity;

        populatedProducts.push({
          productId: item.productId,
          productName: productDetails.name,
          quantity: item.quantity,
        });
      } else {
        wrongProductIds.push(item.productId); // Add wrong product ID to the array
      }
    }

    // Check if there are wrong product IDs
    if (wrongProductIds.length > 0) {
      return res.status(400).json({
        error: "Some product IDs are invalid or not found.",
        wrongProductIds: wrongProductIds,
      });
    }

    // Create a new order with status "pending" (initially)
    const newOrder = new Order({
      userId: userId,
      firstName: user.firstName,
      lastName: user.lastName,
      products: populatedProducts,
      totalAmount: totalAmount,
      status: "pending", // Set the initial status to "pending"
    });

    // Save the new order
    const savedOrder = await newOrder.save();

    // Update product quantities if needed
    for (const item of cartItems) {
      const productDetails = await Product.findById(item.productId);
      if (productDetails) {
        productDetails.quantity -= item.quantity;
        await productDetails.save();
      }
    }

    return res.status(201).json({
      message: "Order created successfully.",
      order: savedOrder,
    });
  } catch (error) {
    console.error("Error creating order:", error); // Log the error
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// Checkout Order (Change status to "processing")
module.exports.checkoutOrder = async (req, res) => {
  try {
    // Find the user's pending order
    const userId = req.user.id;

    const pendingOrder = await Order.findOne({
      userId: userId,
      status: "pending",
    });

    if (!pendingOrder) {
      return res.status(404).json({ error: "Pending order not found" });
    }

    // Update product quantities in the database based on the purchased items
    for (const productInfo of pendingOrder.products) {
      const productId = productInfo.productId;
      const purchasedQuantity = productInfo.quantity;

      // Find the product in the database
      const product = await Product.findById(productId);

      if (!product) {
        return res
          .status(404)
          .json({ error: `Product with ID ${productId} not found` });
      }

      // Check if the available quantity is sufficient
      if (product.quantity < purchasedQuantity) {
        return res.status(400).json({
          error: `Insufficient quantity for product with ID ${productId}`,
        });
      }

      // Subtract the purchased quantity from the available quantity
      product.quantity -= purchasedQuantity;

      // Save the updated product
      await product.save();
    }

    // Change the status of the pending order to "processing"
    pendingOrder.status = "processing";
    await pendingOrder.save();

    return res.status(200).json({ message: "Order checked out successfully" });
  } catch (error) {
    console.error("Error checking out the order:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// Retrieve Authenticated User's Orders
module.exports.getUserOrders = async (req, res) => {
  try {
    const userOrders = await Order.find({ userId: req.user.id });
    return res.status(200).json(userOrders);
  } catch (error) {
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// Retrieve All Orders (Admin Only)
module.exports.getAllOrders = async (req, res) => {
  if (req.user.isAdmin) {
    try {
      const allOrders = await Order.find();
      return res.status(200).json(allOrders);
    } catch (error) {
      return res.status(500).json({ error: "Internal Server Error" });
    }
  } else {
    return res.status(403).json({
      auth: "Failed",
      message: "Action Forbidden. Admin access required.",
    });
  }
};

// Add to Cart
module.exports.addToCart = async (req, res) => {
  try {
    const { productId, quantity = 1 } = req.body;
    const userId = req.user.id;

    // Find the user's "pending" order
    const cartOrder = await Order.findOne({ userId, status: "pending" });

    if (!cartOrder) {
      return res.status(404).json({ error: "Pending cart order not found" });
    }

    // Find the product details
    const productDetails = await Product.findById(productId);
    if (!productDetails) {
      return res.status(404).json({ error: "Product not found" });
    }

    // Add the new product to the cart order only if it's in "pending" status
    if (cartOrder.status === "pending") {
      const newProduct = {
        productId: productId,
        productName: productDetails.name,
        quantity: quantity,
      };
      cartOrder.products.push(newProduct);
      cartOrder.totalAmount += quantity * productDetails.price;

      await cartOrder.save();

      return res
        .status(201)
        .json({ message: "Product added to cart successfully" });
    } else {
      return res
        .status(400)
        .json({ error: "Cannot add to a non-pending order" });
    }
  } catch (error) {
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// Update Cart
module.exports.updateCart = async (req, res) => {
  try {
    const { productId, quantity = 1 } = req.body;
    const userId = req.user.id;

    // Find the user's pending cart (order with status "pending")
    const cartOrder = await Order.findOne({ userId, status: "pending" });

    if (!cartOrder) {
      return res.status(404).json({ error: "Pending cart not found" });
    }

    // Find the product to update in the cart
    const productToUpdate = cartOrder.products.find(
      (product) => product.productId.toString() === productId
    );

    if (productToUpdate) {
      // Find the product's price from the database
      const productDetails = await Product.findById(productId);

      if (!productDetails) {
        return res.status(404).json({ error: "Product not found" });
      }

      // Update the product's quantity
      productToUpdate.quantity = quantity;

      // Calculate the updated total amount
      cartOrder.totalAmount = cartOrder.products.reduce((total, product) => {
        return total + product.quantity * productDetails.price;
      }, 0);

      await cartOrder.save();

      // Send a JSON response with updated cart data
      return res.status(200).json({
        message: "Cart updated successfully",
        cartOrder,
      });
    } else {
      return res.status(404).json({ error: "Product not found in cart" });
    }
  } catch (error) {
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// Remove Cart
module.exports.removeFromCart = async (req, res) => {
  try {
    const productId = req.params.productId;
    const userId = req.user.id;

    // Find the user's pending cart (order with status "pending")
    const cartOrder = await Order.findOne({ userId, status: "pending" });

    if (!cartOrder) {
      return res.status(404).json({ error: "Pending cart not found" });
    }

    // Find the index of the product to remove in the cart
    const productToRemoveIndex = cartOrder.products.findIndex(
      (product) => product.productId.toString() === productId
    );

    if (productToRemoveIndex !== -1) {
      // Product found in the cart, remove it
      const productToRemove = cartOrder.products[productToRemoveIndex];

      // Find the product's price from the database
      const productDetails = await Product.findById(productId);

      if (!productDetails) {
        return res.status(404).json({ error: "Product not found" });
      }

      // Update the cart order by removing the product
      cartOrder.products.splice(productToRemoveIndex, 1);

      // Calculate the updated total amount
      cartOrder.totalAmount -= productToRemove.quantity * productDetails.price;

      // If the quantity becomes 0 or less, remove the product from the cart completely
      if (productToRemove.quantity <= 0) {
        cartOrder.products.pull(productToRemove._id);
      }

      await cartOrder.save();

      return res
        .status(200)
        .json({ message: "Product removed from cart successfully" });
    } else {
      return res.status(404).json({ error: "Product not found in cart" });
    }
  } catch (error) {
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// Get Order Status by Order ID (Admin Only)
module.exports.getOrderStatus = async (req, res) => {
  if (req.user.isAdmin) {
    try {
      const orderId = req.params.orderId;

      // Find the order by ID and return its status
      const order = await Order.findById(orderId);

      if (!order) {
        return res.status(404).json({ error: "Order not found" });
      }

      return res.status(200).json({ status: order.status });
    } catch (error) {
      return res.status(500).json({ error: "Internal Server Error" });
    }
  } else {
    return res.status(403).json({
      auth: "Failed",
      message: "Action Forbidden. Admin access required.",
    });
  }
};

// Update Order Status (Admin Only)
module.exports.updateOrderStatus = async (req, res) => {
  try {
    const { newStatus } = req.body;
    const orderId = req.params.orderId; // Retrieve orderId from URL

    // Define the valid status transitions
    const statusTransitions = {
      pending: ["processing"],
      processing: ["packing"],
      packing: ["in transit"],
      "in transit": ["delivered"],
    };

    // Fetch the order by ID
    const order = await Order.findById(orderId);

    if (!order) {
      return res.status(404).json({ error: "Order not found" });
    }

    if (order.status === "delivered") {
      // If the order is already delivered, it cannot be edited
      return res.status(403).json({
        error: "Order has been delivered and cannot be edited",
      });
    }

    if (!statusTransitions[order.status]) {
      return res.status(400).json({ error: "Invalid current status" });
    }

    if (statusTransitions[order.status].includes(newStatus)) {
      // Update the order status if the transition is valid
      order.status = newStatus;
      await order.save();
      return res.status(200).json(order);
    } else {
      return res.status(400).json({
        error: "Invalid status transition",
        validTransitions: statusTransitions[order.status],
      });
    }
  } catch (error) {
    console.error("Error updating order status:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// Retrieve Authenticated User's Pending Cart
module.exports.getMyCart = async (req, res) => {
  try {
    // Query the database to find the user's pending cart
    const pendingCart = await Order.findOne({
      userId: req.user.id,
      status: "pending",
    });

    res.json(pendingCart);
  } catch (error) {
    console.error("Error fetching pending cart:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

// Clear Cart (Non Admin Only)
module.exports.clearCart = async (req, res) => {
  try {
    const userId = req.user.id; // Get the user ID from the authenticated user

    console.log("Clear Cart Request Received for User ID:", userId);

    // Find the cart order for the user
    const cartOrder = await Order.findOne({ userId });

    if (!cartOrder) {
      console.log("Cart not found for User ID:", userId);
      return res.status(404).json({ error: "Cart not found" });
    }

    console.log("Clearing Cart for User ID:", userId);

    // Clear the cart by setting the products array to an empty array
    cartOrder.products = [];

    // Set the total amount to 0
    cartOrder.totalAmount = 0;

    await cartOrder.save();

    console.log("Cart cleared successfully for User ID:", userId);

    return res.status(200).json({ message: "Cart cleared successfully" });
  } catch (error) {
    console.error("Error clearing the cart:", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};
