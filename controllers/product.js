// Dependencies

const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create New Product
module.exports.createProduct = (req, res) => {
  let newProduct = new Product({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    quantity: req.body.quantity,
    imageUrl: req.body.imageUrl, // Include imageUrl when creating a new product
  });

  newProduct
    .save()
    .then((product) => {
      // Send a success response with the created product
      const createdProduct = {
        name: product.name,
        description: product.description,
        price: product.price,
        quantity: product.quantity,
        imageUrl: product.imageUrl, // Include imageUrl in the response
      };
      return res.status(201).json(createdProduct);
    })
    .catch((err) => {
      // Log the error for debugging purposes
      console.error("Error creating product:", err);

      // Send an error response with a message
      return res
        .status(500)
        .json({ error: "Could not create product", message: err.message });
    });
};

// Retrieve All Products (Admin Only)
module.exports.getAllProducts = (req, res) => {
  return Product.find({})
    .then((result) => {
      return res.send(result);
    })
    .catch((err) => res.send(err));
};

// Retrieve All Active Products
module.exports.getAllActiveProducts = (req, res) => {
  return Product.find({ isActive: true }).then((result) => {
    return res.send(result);
  });
};

// Retrieve Specific Product
module.exports.getProduct = (req, res) => {
  return Product.findById(req.params.productId).then((result) => {
    return res.send(result);
  });
};

// Update a Specific Product (Admin Only)
module.exports.updateProduct = (req, res) => {
  let updatedProduct = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    quantity: req.body.quantity,
    imageUrl: req.body.imageUrl, // Include imageUrl in the update
  };

  return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then(
    (product, error) => {
      if (error) {
        return res.send(false);
      } else {
        res.send(true);
      }
    }
  );
};

// Archive Product (Admin Only)
module.exports.archiveProduct = (req, res) => {
  return Product.findByIdAndUpdate(req.params.productId, { isActive: false })
    .then((product) => {
      if (product) {
        return res.send(true);
      } else {
        return res.send(false);
      }
    })
    .catch((error) => {
      return res.send(false);
    });
};

// Activate Product (Admin Only)
module.exports.activateProduct = (req, res) => {
  const productId = req.params.productId;

  return Product.findByIdAndUpdate(productId, { isActive: true }).then(
    (product, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    }
  );
};

// Search Products by Name
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    const products = await Product.find({
      name: { $regex: productName, $options: "i" },
      isActive: true,
    });

    if (products.length === 0) {
      return res.send("No such product found.");
    }

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json("Internal Server Error");
  }
};
